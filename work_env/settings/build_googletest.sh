NOW=$(date +"%y-%m-%d %T")
PWD=`pwd -P`
SH_PATH="$BASH_SOURCE"
SH_DIR="$( cd "$( dirname "$0" )" && pwd -P )"

echo "[LOG: $NOW] Run script: $SH_PATH"
echo "[LOG: $NOW] build googletest"
cd ~/googletest
cmake -H. -Bbuild
cd build
make -j
echo "[LOG: $NOW] build gtest example"
echo "[LOG: $NOW] gtest example path: $SH_DIR/../GTest"
cd $SH_DIR/../GTest
cmake -H. -Bbuild
cd build
make -j
echo "[LOG: $NOW] gtest example output path: $PWD/gtest.exe"
