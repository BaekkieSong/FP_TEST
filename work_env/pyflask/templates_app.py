from flask import Flask, send_file, request, render_template, jsonify
import os

app = Flask(__name__)

@app.route('/', methods = ['GET', 'POST'])
def home():
  return render_template('text.html')
@app.route('/subtext', methods = ['GET', 'POST'])
def subtext():
  return render_template('subtext.html')
@app.route('/gtm', methods = ['GET', 'POST'])
def gtm():
  return render_template('gtm.js')

@app.route('/local', methods = ['GET', 'POST'])
def local():
  return render_template('local.html')
@app.route('/recieve_data')
def get_id():
   the_id = request.args.get('button_id')
   return "<p>Got it!</p>"

@app.route('/jsondata', methods = ['GET', 'POST'])
def returnjson():
  return jsonify(upload='success', data='aaaaaa')

if __name__ == '__main__':
  app.run(host='0.0.0.0', port=3000)

