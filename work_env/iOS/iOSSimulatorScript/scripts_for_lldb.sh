## 1. Get the device UDID

```
$ xcrun simctl list devices 
```
* 사용 가능한 Device 들과 그에 해당하는 UDID 를 조회 할 수 있다.
```
== Devices ==
-- iOS 15.2 --
    iPhone 6s (A0360A26-DED1-445D-9BF2-31546718DF49) (Booted)
    iPhone 8 (305719BF-E03B-4CAA-B102-0E6F76BE1B7A) (Shutdown)
    iPhone 8 Plus (F0C74C8A-A373-4185-8C62-DAACA4E2E0C5) (Shutdown)
    iPhone 11 (E432AB4A-1535-4419-8498-D54FE5D93BD2) (Shutdown)
    iPhone 11 Pro (DB505D51-D8CF-4AB3-95DB-2D9E0D362A70) (Shutdown)
...
```

## 2. Launch the Simulator

```
$ open -a Simulator --args -CurrentDeviceUDID A0360A26-DED1-445D-9BF2-31546718DF49
```
* 원하는 Device 의 UDID 를 입력하여 Simulator를 실행한다.
  * 여기서는 iPhone6s 를 실행 하였다.

## 3. Install the App
```
xcrun simctl install booted [Chromium.app]
```
* 현재 Boot 된 Device에 iOS용으로 생성된 App을 설치한다.
  * [Chromium.app] 은 현재 경로에서 App의 Path를 의미한다
  * Ex) [Chromium.app] = ~/super-os-desktop/src/out/Debug-iphonesimulator/Chromium.app

## 4. Launch LLDB and wait for the App to Launch

```
$ lldb
```
* lldb 실행
```
(lldb) platform select ios-simulator
```
* lldb에서 ios-simulator 전용으로 설정
  * [A0360A26-DED1-445D-9BF2-31546718DF49]: 사용할 device의 UDID
```
(lldb) platform connect [A0360A26-DED1-445D-9BF2-31546718DF49]
```

* 위에서 실행한 Simulator의 UDID(iPhone6s)를 입력하여 연결
```
(lldb) process attach -n [Chromium] --waitfor
```
* [Chromium]의 Process 가 실행될때 까지 기다린다.

## 5. Launch App
* 앱을 실행하면 lldb에서 Process가 Attach 됨을 확인 할 수 있다.

## 6. Use Script
* 매번 lldb에 command를 넣기 귀찮기 때문에 아래와 같이 script를 작성하여 입력할 수 있다
```
platform select ios-simulator
platform connect A0360A26-DED1-445D-9BF2-31546718DF49
process attach -n Chromium --waitfor
br s -F main
process continue
```
* 위와 같은 스크립트를 작성한 뒤 lldb 실행시 인자로 입력해주면 된다.
```
$ lldb --source [~/lldb-iossim-debug.cmd]
```
* [~/lldb-iossim-debug.cmd] 는 script의 경로


[참조]
* http://junch.github.io/debug/2016/09/19/original-lldb.html
