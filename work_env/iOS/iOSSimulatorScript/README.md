# lldb환경에서 iOS Simulator Debugging 설정 방법
## 스크립트 설명
* iOS Simulator를 lldb를 실행할 플랫폼 환경으로 설정
  * `platform select ios-simulator`
* iOS Simulator 기기 UDID 설정
  * `platform connect 6D4749BD-BF2F-4133-A007-EBF037D0EC0D`
* breakpoint 설정(예시)
  * <pre><code>b page_allocator_internals_posix.h:172
               b ios_chrome_main_parts.mm:153
               #b l10n_util_mac.mm:63
               b SystemAllocPagesInternal
               b IOSChromeMainDelegate::BasicStartupComplete</code></pre>
* runtime args 설정(잘 동작하지 않음..)
  * `#settings set target.run-args local.html`
* iOS Simulator에서 앱(Chromium.app)이 실행될 때까지 프로세스 attach 대기
  * `process attach -n Chromium --waitfor`
* main함수에서 한 번 멈추도록 break설정
  * `br s -F main`
  * 주의 - attach가 오래걸리면 breakpoint설정들을 건너뛸 수 있음
* Debugging시작
  * `#process continue`
  * 주의 - attach전에 커맨드입력되면 처리되어 breakpoint설정들을 건너뛸 수 있음


