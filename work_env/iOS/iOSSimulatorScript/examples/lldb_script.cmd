platform select ios-simulator
platform connect 6D4749BD-BF2F-4133-A007-EBF037D0EC0D 

##b local_frame_view.cc:288
##b local_frame_view.cc:235
##b document_loader.cc:1527
##b browser_ui_thread_scheduler.cc:117
##b message_pump_mac.cc:876
##b navigation_client.cc:45

##b render_frame_host_impl.cc:10374
##b url_request.cc:581
##b url_request_http_job.cc:247
##b render_frame_host_impl.cc:7826
##b blink_platform_impl.cc:162
##b document_loader.cc:1567
##b dobby_browser_main_parts.cc:84
##b url_loader.cc:1362
##b NavigationClientStubDispatch::AcceptWithResponder
##b render_frame_impl.cc:5063
##b render_frame_impl.cc:5897
##b render_frame_impl.cc:2126
###b associated_interface_registry.cc:15
##b associated_interface_registry.cc:26
##b agent_scheduling_group.cc:291
##b ipc_channel_mojo.cc:261
##b blink::V8Initializer::InitializeMainThread
#
##b dobby_content_browser_client.cc:197
##b navigation_request.cc:2164
##b navigation_request.cc:1298
##b render_frame_host_impl.cc:3212
##b render_frame_host_impl.cc:4643
#
## ---------------------- Fonts ----------------------
##b FontCache::CreateFontPlatformData
##b FontCache::GetFontPlatformData
##b FontCache::FontDataFromFontPlatformData
##b font_cache.cc:155
##b FontCache::PlatformFontFallbackFontForCharacter
##b font_platform_data_dobby.mm:175
##b font_platform_data_dobby.mm:222
### alter font name unique check
##b font_cache.cc:186
### check font_name == @"LastResort". cur == false
##b font_platform_data_dobby.mm:105
##b FontPlatformData::FontPlatformData
### for get system font
##b WebSandboxSupportMac::LoadFont
##b FontLoader::CTFontDescriptorFromBuffer
##b font_loader.mm:49
### check font data cached here.
##b font_data_cache.cc:62
### check do not be created simplefontdata matched fontplatformdata
##b font_data_cache.cc:78
### no fontdata and get last resort. it's not expected!!!
##b font_fallback_list.cc:203
##b font_platform_data_dobby.mm:59
##b SkTypeface_mac_ct.cpp:1297
##b font_cache.cc:295
##b font_fallback_iterator.mm:287
##b font_fallback_list.cc:254
#
## ----------------------- Blink Layer ----------------
##b pre_paint_tree_walk.cc:176
##b root_compositor_frame_sink_impl.cc:483
##b host_display_client.cc:45
##b host_display_client.cc:28
##b renderer_settings_creation.cc:77
##b RemoteLayerAPISupported
## ------ CALayer --------
##b render_widget_host_ui_view_bridge.mm:28
##b dobby_platform_delegate_ios.mm:164
##b render_widget_host_view_dobby.mm:179
##b web_contents_view_dobby.mm:166
##b render_widget_host_ui_view_bridge.mm:82
## ------ CALayer Rendering --------
##b software_output_device_dobby.cc:29
##b software_output_device_dobby.cc:53
##b display_ca_layer_tree.mm:67
##b UpdateCALayerTree
##b delay_based_time_source.cc:84
##b display.cc:915
##b skia_output_surface_impl.cc:1110
##b display_scheduler.cc:97
##b software_renderer.cc:136  #outputsurface
##b software_renderer.cc:147  #texture
##b render_widget_host_view_dobby.mm:615
##b render_widget_host_view_dobby.mm:86
##b render_widget_host_view_dobby.mm:1303
##b render_widget_host_view_dobby.mm:1321
##b navigation_client.cc:68
##b DidReceiveCALayerParams
## browser_compositor_dobby.mm:114
## -------------- HTML Parser -----------------------
##b FrameLoader::Init
#b DocumentLoader::DocumentLoader
##b DocumentLoader::CommitNavigation
##b DocumentLoader::DidCommitNavigation
##b DocumentLoader::CreateParserPostCommit
## DocumentLoader is recreated after FrameLoader::CommitNavigation
#b FrameLoader::CommitNavigation
#b NavigationBodyLoader::StartLoadingBody
#b NavigationBodyLoader::OnStartLoadingResponseBody
## from LocalFrame::ForceSynchronousDocumentInstall maybe.
#b BackgroundHTMLParser::AppendRawBytesFromMainThread
## -> PumpTokenizer
#b HTMLParserScheduler::ScheduleForUnpause
## no called.
#b html_document_parser.cc:1694
#b html_document_parser.cc:1707
## pre caller: ContinueParsing
##b HTMLDocumentParser::ConstructTreeFromCompactHTMLToken
##b HTMLTreeBuilder::ConstructTree
#b ParseError
## ProcessToken -> { ProcessDoctypeToken, ProcessStartTag, ProcessEndTag, .., ProcessEndOfFile}
## ProcessStartTag -> { kInitialMode, kBeforeHeadMode, kInBodyMode, kInTableMode, ...}
## kInBodyMode -> ProcessStartTagForInBody -> HTMLConstructionSite::InsertHTMLElement -> Document::DocumentRawElement -> HTMLElementFactory::Create -> HTMLDivConstructor -> HTMLDivElement
## ! need to check bodyelement call order.
#b HTMLBodyElement::HTMLBodyElement
#b HTMLDivElement::HTMLDivElement
##b HTMLParserScheduler::ScheduleForUnpause called 1more.
## cc::LayoutTreeHost::RequestMainFrameUpdate
##UpdateLifecycle
##b LocalFrameView::UpdateStyleAndLayout
#b Document::UpdateStyleAndLayoutTreeThisDocument
#b Document::UpdateStyle
## check IsJavascriptURL is true
#b document_loader.cc:2417
#b Document::PushCurrentScript
#b Document::currentScriptForBinding
#b HTMLScriptElement::IsScriptElement
#b HTMLScriptElement::HTMLScriptElement
## Read body data from url_loader
#b HTMLNoScriptElement::LayoutObjectIsNeeded
#b HTMLNoScriptElement::LayoutObjectIsNeeded
#b HTMLScriptElement::setText
#b pre_paint_tree_walk.cc:123
#b TextFinder::Find
## Maybe Set HttpBody to DocumentLoader
#b DocumentLoader::SetHistoryItemStateForCommit
#b Node::showTree
#b LayoutObject::showTree
#b LayoutObject::CreateObject
#b NGLayoutAlgorithm::NGLayoutAlgorithm
#
##b ScrollbarThemeDobby::RegisterScrollbar
##b ScrollbarThemeDobby::ScrollbarThemeDobby
#
##b ComputedStyle::ComputedStyle
## check *state.Style().display_ is 0(kInline). 1(kBlock). state.Style()->Display()
#b style_resolver.cc:1455
## 0(kInline) at this time.
#b style_resolver.cc:854
## check Element with Style. 0 -> 1 in this API.
#b AdjustComputedStyle
#b AdjustStyleForHTMLElement
#b AdjustStyleForDisplay
#b element.cc:3067
#b ComputedStyle::IsDisplayInlineType
#b ComputedStyleBase::SetDisplay
#b ComputedStyleBase::SetOriginalDisplay
#b LayoutObject::SetIsAtomicInlineLevel

#b LayoutObject::SetInline
#b LayoutObject::SetIsBox
#b LayoutObject::SetIsText
#b LayoutObject::IsBox
## IsMobileDocument == true check
#b Document::SetDoctype
#b DocumentLoader::DocumentLoader
## Scroll Viewport's is_box == true callback
#b local_frame_view.cc:1999
#b ComputedStyleBase::ResetDisplay
#b ComputedStyleBase::ResetOriginalDisplay
#b ViewportStyleResolver::CollectViewportRulesFromUASheets
#b NavigationBodyLoader::OnStartLoadingResponseBody
## SetLayoutNGEnabled, SetLayoutNGFragmentEnabled
#b V8ContextSnapshotImpl::TakeSnapshot
#b LayoutNGEnabled
#b LayoutNGFragmentEnabled
#b layout_object_factory.cc:103
#b SetComputedStyle
#b HTMLBodyElement::HTMLBodyElement
#b Element::StyleForLayoutObject
#b NodeRenderingData::SetComputedStyle
#b LayoutNGBlockInInlineEnabled
#b LayoutNGPrintingEnabled
#b LayoutNGEnabled
#b LayoutNGBlockFragmentationEnabled

#b CSSDefaultStyleSheets::InitializeDefaultStyles
b CSSDefaultStyleSheets::CSSDefaultStyleSheets
#b ComputedStyle::ComputedStyle
#b StyleResolverState
#b ComputedStyle::SetDisplay
#b StyleResolver::StyleResolver
#b LayoutObject::LayoutObject
#b StyleBuilder::ApplyProperty
#b StyleCascade::ApplyMatchResult
#b MatchResult::Expansions
#b MatchResult::AddMatchedProperties

#b StyleResolver::MatchUARules
#b CSSDefaultStyleSheets::InitializeDefaultStyles
#b CSSParser::ParseSheet
#b CSSParser::ParseSheetForInspector
#b css_default_style_sheet.cc:94
b Document::Document
b ResourceBundle::ResourceBundle
b InitializeResourceBundle
b ResourceBundle::GetRawDataResourceForScale
b SetICUDefaultLocale
b ResourceBundle::GetLocaleFilePath

b PartitionAddressSpace::Init
b InitializeAllocatorShim
b InitializeDefaultAllocatorPartitionRoot
#b ShimMalloc
b SystemAllocPagesInternal



#settings set target.run-args local.html
process attach -n Chromium --waitfor
br s -F main
#process continue


