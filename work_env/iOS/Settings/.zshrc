# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="agnoster"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment one of the following lines to change the auto-update behavior
# zstyle ':omz:update' mode disabled  # disable automatic updates
# zstyle ':omz:update' mode auto      # update automatically without asking
# zstyle ':omz:update' mode reminder  # just remind me to update when it's time

# Uncomment the following line to change how often to auto-update (in days).
# zstyle ':omz:update' frequency 13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# You can also set it to another string to have that shown instead of the default red dots.
# e.g. COMPLETION_WAITING_DOTS="%F{yellow}waiting...%f"
# Caution: this setting can cause issues with multiline prompts in zsh < 5.7.1 (see #5765)
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git)


# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
#
#
export HOME="/Users/tmax"
export OUT="${HOME}/super-os-desktop/src/out/Debug-iphonesimulator"
alias src="cd ${HOME}/super-os-desktop/src/"
alias out="cd ${HOME}/super-os-desktop/src/out/Debug-iphonesimulator"
alias bb="autoninja -C ${HOME}/super-os-desktop/src/out/Debug-iphonesimulator"
alias bbc="autoninja -C ${HOME}/super-os-desktop/src/out/Debug-iphonesimulator chrome"

alias vz="vim ${HOME}/.zshrc"
alias sz="source ${HOME}/.zshrc"
export PATH="$PATH:${HOME}/depot_tools"

POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(user dir vcs)

POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status)

export APP_PATH="${HOME}/super-os-desktop/src/out/Debug-iphonesimulator/Chromium.app"
export UDID="6D4749BD-BF2F-4133-A007-EBF037D0EC0D"  #iphone s6
#export UDID="C57D095D-127A-4DFA-8C72-AEFE1ED236F3" #iphone 8
# LLDB Debug Test for Chromium
alias xc_list="xcrun simctl list devices"
alias xc_select="open -a Simulator --args -CurrentDeviceUDID ${UDID}"
alias xc_install="xcrun simctl install booted ${APP_PATH}"
alias xc_lldb="lldb  --source ${HOME}/lldb_script.cmd"
#alias xc_select="platform select ios-simulator"
#alias xc_connectid="platform connect ${UDID}
alias xc_test="xc_select && xc_install && xc_lldb"

export APP_PATH_ORIGIN="${HOME}/chromium/src/out/Debug-iphonesimulator/Chromium.app"
alias xc_install_origin="xcrun simctl install booted ${APP_PATH_ORIGIN}"
alias xc_lldb_origin="lldb  --source ${HOME}/lldb_script.cmd_origin.cmd"
alias xc_test_origin="xc_select && xc_install_origin && xc_lldb_origin"

alias rmlog="rm -r ${HOME}/Library/Developer/Xcode/DerivedData/temporary-*"
alias run="${OUT}/iossim ${OUT}/Chromium.app"
alias run_blink="${OUT}/iossim -c \"--v=3 --vmodule=*pre_paint_tree_walk*=3 --disable-blink-features=LayoutNG\" ${OUT}/Chromium.app"
alias pk="kill -9" # <= killall # 프로세스 이름으로도 삭제 가능
alias pse="ps -ef"
alias pseg="ps -ef | grep "
alias pkch="pkill -9 Chromium lldb"
export CHROMIUM_BUILDTOOLS_PATH=${HOME}/super-os-desktop/src/buildtools
alias xc_device="xcrun xctrace list devices"
# iphone udid = 81609a8960b6b2c6c38c0b35b8748806111dca67
# brew install libimobliedevice
# brew install ideviceinstaller
alias ios_install="ideviceinstaller -i " # [.app path]

alias findid="xcrun security find-identity -v -p codesigning"
