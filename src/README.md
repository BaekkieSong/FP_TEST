# Test for Develop Cpp
## Vim Hotkey Tips
### edit
* to line begin/back : 0/$
* insert from Back : A
* insert from previous/next line : O/o
* delete char : x/X
### visual mode
* vim *.cc/h
* press 'v'
* line selection: shift + v
* block selection: ctrl + v
* all selecion: gg, v, shift + g
#### tips
* to '}' : % on '{'
* folding : %zf on '{/}'
* open fold : zo
* search same strings : * or #
* confirm func definition : [i
* confirm variables : gd
* tab line : >> or <<
* source arrange : = or ==
* open header : ctrl + w, f
### ctags
* tagging specific : ctags * or ctags name
* tagging all subdirectory : ctags -R
* [+Warning+] set tags path at the .vimrc file
#### tips
* jump to definition : ctrl + ]
* to before tags : ctrl + t
* to previous : tp
* to next : tn
* to first : tr
* to last : tl
#### tips2
* vim -t keyword keyword가 있는 파일 열기
* :ta keyword keyword 와 일치하는 태그 위치로 이동
* :ta /keyword keyword 가 포함된 태그 검색
* :tj keyword keyword 와 일치하는 태그 목록을 출력하고 선택하여 이동 (일치하는 태그가 한개일 경우 바로 이동)
